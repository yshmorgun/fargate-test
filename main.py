from flask import Flask
app = Flask(__name__)

@app.route('/')
def handler():
	return 'This is my test. You didn\'t submit anything'

@app.route('/<data>')
def hello_world(data):
    return 'This is my test. You submitted: <strong>{}</strong>'.format(data)